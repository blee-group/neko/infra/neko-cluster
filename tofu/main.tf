variable "project_id" {
  description = "GCP project ID"
  type        = string
}

variable "region" {
  description = "GCP region"
  type        = string
}

# cluster regions are different than default eg: us-west1 vs us-west-1
variable "cluster_region" {
  description = "GCP cluster region"
  type        = string
}

provider "google" {
  credentials = "/tmp/service-account-key.json"
  project     = var.project_id
  region      = var.region
}

resource "google_container_cluster" "neko-cluster" {
  name     = "neko-cluster"
  location = var.cluster_region

  release_channel {
    channel = "REGULAR"
  }

  # Enable AutoPilot mode
  enable_autopilot = true

  # Vertical pod autoscaling
  vertical_pod_autoscaling {
    enabled = true
  }

  resource_labels = {
    "group" : "neko",
    "env" : "demo"
  }

  # Disable deletion protection
  deletion_protection = false
}
