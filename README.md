
# Neko Cluster Project

This project deploys the cluster via Terraform using the [GitLab OpenTofu](https://gitlab.com/components/opentofu) component

## Usage

Based on the GitLab maintained OpenTofu component.

This project deploys the parent cluster for the `Neko` app group. Applications
under the `app` directories will have privileges to deploy to this cluster while
the cluster provisioning itself is managed outside of the application groups
